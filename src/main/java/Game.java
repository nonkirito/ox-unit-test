


import java.io.IOException;
import java.util.*;

public class Game {

    private Table table;
    private int row;
    private int col;
    private Player o;
    private Player x;
    Scanner scan = new Scanner(System.in);

    public Game() {
        o = new Player('O');
        x = new Player('X');
    }

    public void startGame() {
        table = new Table(o, x);
        table.createData();
    }

    public void showWelcome() {
        System.out.println(" ------------------\n" + "|Welcome to OX Game|\n" + " ------------------");
    }

    public void showTable() {
        char dataTable[][] = table.getData();
        for (int i = 0; i < 3; i++) {
            for (int j = 0; j < 3; j++) {
                System.out.print(dataTable[i][j] + " ");
            }
            System.out.println();
        }
    }

    public void showTurn() {
        System.out.println(table.getCurrentPlayer().getName() + " turn");
    }

    public void showInputRowCol() {
        System.out.print("Please input Row and Column: ");
    }

    public void showInputErrorNotNumber() {
        System.out.println("Please input number only!");
    }

    public void showNotInputInRange() {
        System.out.println("Please input again. Input is out of range.");
    }

    public void showInputErrorRepeat() {
        System.out.println("Pleae input again. This column is not available.");
    }

    public boolean inputRowCol() {
        boolean checkloopinput = true;
        while (checkloopinput) {
            try {
                showInputRowCol();
                int row = scan.nextInt();
                int col = scan.nextInt();
                if (checkInput(row, col)) {
                    table.setRowCol(row, col);
                    checkloopinput = false;
                    return true;
                }
            } catch (Exception e) {
                showInputErrorNotNumber();
                scan.nextLine();
                continue;
            }
        }
        return false;
    }

    public boolean checkInput(int row, int col) {
        char data[][] = table.getData();
        if (row > 3 || col > 3) {
            showNotInputInRange();
            return false;
        } else if (data[row - 1][col - 1] == '-') {
            return true;
        } else {
            showInputErrorRepeat();
            return false;
        }
    }

    public void showInputContinue() {
        System.out.print("Continue? (y / n): ");
    }

    public void showInputContinueError() {
        System.out.println("Please input (y / n)");
    }

    public boolean inputContinue() {
        boolean checkLoop = true;
        while (checkLoop) {
            showInputContinue();
            char checkContinue = scan.next().charAt(0);
            if (checkContinue == 'y') {
                checkLoop = false;
                return true;
            } else if (checkContinue == 'n') {
                return false;
            } else {
                showInputContinueError();
            }
        }
        return false;
    }

    public void showWin(Player player) {
        showTable();
        if (player == o || player == x) {
            System.out.println("Player " + player.getName() + " Win!");
        }
    }

    public void showDraw(Player player) {
        showTable();
        if (player == null) {
            System.out.println("Draw!");
        }
    }

    public void showStats() {
        System.out.println("O: Win: " + o.getWin() + " Lose: " + o.getLose() + " Draw: " + o.getDraw());
        System.out.println("X: Win: " + x.getWin() + " Lose: " + x.getLose() + " Draw: " + x.getDraw());
    }

    public void showFinish() {
        System.out.println("--- Game End. Bye Bye ---");
    }

    public void run() {
        showWelcome();
        while (true) {
            startGame();
            playGame();
            if (!inputContinue()) {
                break;
            }
        }
        showFinish();
    }

    public void playGame() {
        while (true) {
            showTable();
            showTurn();
            if (inputRowCol()) {
                if (table.checkWin()) {
                    Player player = table.getWinner();
                    showWin(player);
                    showStats();
                    break;
                } else if (table.checkDraw()) {
                    Player player = table.getWinner();
                    showDraw(player);
                    showStats();
                    break;
                } else {
                    table.switchTurn();
                }
            }
        }
    }
}
