
import java.util.Random;

public class Table {

    private char data[][] = new char[3][3];
    private Player currentPlayer;
    private Player o;
    private Player x;
    private Player win;
    private int countTurn;
    private int lastcol;
    private int lastrow;
    Random randomPlayer = new Random();

    public Table(Player o, Player x) {
        this.o = o;
        this.x = x;
        win = null;
        if (randomPlayer.nextInt(2) == 0) {
            currentPlayer = o;
        } else {
            currentPlayer = x;
        }
    }

    public void createData() {
        for (int i = 0; i < 3; i++) {
            for (int j = 0; j < 3; j++) {
                data[i][j] = '-';
            }
        }
    }

    public void setRowCol(int row, int col) {
        if (data[row - 1][col - 1] == '-') {
            data[row - 1][col - 1] = currentPlayer.getName();
            lastcol = col - 1;
            lastrow = row - 1;
            countTurn++;
        }

    }

    public boolean checkWin() {
        for (int i = 0; i < 3; i++) {
            if (checkVertical(i)) {
                return true;
            }
            if (checkHorizontal(i)) {
                return true;
            }
            if (checkDiagonal1()) {
                return true;
            }
            if (checkDiagonal2()) {
                return true;
            }
        }
        return false;
    }

    public Player getWinner() {
        return win;
    }

    public void setWinLose() {
        if (getWinner() == o) {
            o.win();
            x.lose();
        } else if (getWinner() == x) {
            x.win();
            o.lose();
        }
    }

    public void switchTurn() {
        if (currentPlayer == o) {
            currentPlayer = x;
        } else {
            currentPlayer = o;
        }
    }

    public Player getCurrentPlayer() {
        return currentPlayer;
    }

    public char[][] getData() {
        return data;
    }

    private boolean checkHorizontal(int row) {
        int count = 0;
        for (int i = 0; i < 3; i++) {
            if (data[row][i] == currentPlayer.getName()) {
                count++;
            }
        }
        if (checkCount(count)) {
            return true;
        }
        return false;
    }

    private boolean checkVertical(int col) {
        int count = 0;
        for (int i = 0; i < 3; i++) {
            if (data[i][col] == currentPlayer.getName()) {
                count++;
            }
        }
        if (checkCount(count)) {
            return true;
        }
        return false;
    }

    private boolean checkDiagonal1() {
        int count = 0;
        for (int i = 0; i < 3; i++) {
            if (data[i][i] == currentPlayer.getName()) {
                count++;
            }
        }
        if (checkCount(count)) {
            return true;
        }
        return false;
    }

    private boolean checkDiagonal2() {
        int count = 0;
        for (int i = 0; i < 3; i++) {
            if (data[2 - i][i] == currentPlayer.getName()) {
                count++;
            }
        }
        if (checkCount(count)) {
            return true;
        }
        return false;
    }

    private boolean checkCount(int count) {
        if (count == 3) {
            win = currentPlayer;
            setWinLose();
            return true;
        }
        return false;
    }

    public boolean checkDraw() {
        if (countTurn == 9) {
            o.draw();
            x.draw();
            win = null;
            return true;
        }
        return false;
    }

}
