
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

public class TableUnitTest {

    public void testRow1Win() {
        Player o = new Player('O');
        Player x = new Player('X');
        Table table = new Table(o, x);
        table.createData();
        table.setRowCol(1, 1);
        table.setRowCol(1, 2);
        table.setRowCol(1, 3);
        assertEquals(true, table.checkWin());
    }
    public void testRow2Win(){
        Player o = new Player('O');
        Player x = new Player('X');
        Table table = new Table(o, x);
        table.createData();
        table.setRowCol(2, 1);
        table.setRowCol(2, 2);
        table.setRowCol(2, 3);
        assertEquals(true, table.checkWin());
    }
    public void testRow3Win(){
        Player o = new Player('O');
        Player x = new Player('X');
        Table table = new Table(o, x);
        table.createData();
        table.setRowCol(3, 1);
        table.setRowCol(3, 2);
        table.setRowCol(3, 3);
        assertEquals(true, table.checkWin());
    }
    public void testCol1Win(){
        Player o = new Player('O');
        Player x = new Player('X');
        Table table = new Table(o, x);
        table.createData();
        table.setRowCol(1, 1);
        table.setRowCol(2, 1);
        table.setRowCol(3, 1);
        assertEquals(true, table.checkWin());
    }
    public void testCol2Win(){
        Player o = new Player('O');
        Player x = new Player('X');
        Table table = new Table(o, x);
        table.createData();
        table.setRowCol(1, 2);
        table.setRowCol(2, 2);
        table.setRowCol(3, 2);
        assertEquals(true, table.checkWin());
    }
    public void testCol3Win(){
        Player o = new Player('O');
        Player x = new Player('X');
        Table table = new Table(o, x);
        table.createData();
        table.setRowCol(1, 3);
        table.setRowCol(2, 3);
        table.setRowCol(3, 3);
        assertEquals(true, table.checkWin());
    }
    public void testDiagonal1Win(){
        Player o = new Player('O');
        Player x = new Player('X');
        Table table = new Table(o, x);
        table.createData();
        table.setRowCol(1, 1);
        table.setRowCol(2, 2);
        table.setRowCol(3, 3);
        assertEquals(true, table.checkWin());
    }
    public void testDiagonal2Win(){
        Player o = new Player('O');
        Player x = new Player('X');
        Table table = new Table(o, x);
        table.createData();
        table.setRowCol(1, 3);
        table.setRowCol(2, 2);
        table.setRowCol(3, 1);
        assertEquals(true, table.checkWin());
    }
    public void testDraw(){
        Player o = new Player('O');
        Player x = new Player('X');
        Table table = new Table(o, x);
        table.createData();
        table.setRowCol(1, 1);
        table.switchTurn();
        table.setRowCol(1, 2);
        table.switchTurn();
        table.setRowCol(1, 3);
        table.switchTurn();
        table.setRowCol(2, 2);
        table.switchTurn();
        table.setRowCol(2, 3);
        table.switchTurn();
        table.setRowCol(3, 3);
        table.switchTurn();
        table.setRowCol(3, 2);
        table.switchTurn();
        table.setRowCol(3, 1);
        table.switchTurn();
        table.setRowCol(2, 1);
        table.switchTurn();
        assertEquals(true, table.checkDraw());
    }
    public void testSwitchTurn(){
        Player o = new Player('O');
        Player x = new Player('X');
        Table table = new Table(o, x);
        table.createData();
        table.switchTurn();
        if(table.getCurrentPlayer().getName() == 'X'){
            table.switchTurn();
        }
        assertEquals('O', table.getCurrentPlayer().getName());
    }
}
